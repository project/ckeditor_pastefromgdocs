<?php

namespace Drupal\ckeditor_pastefromgdocs\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Paste from Google Docs" plugin.
 *
 * @CKEditorPlugin(
 *   id = "pastefromgdocs",
 *   label = @Translation("Paste from Google Docs")
 * )
 */
class CKEditorPastefromgdocs extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/ckeditor/plugins/pastefromgdocs/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = drupal_get_path('module', 'ckeditor_pastefromgdocs') . '/icons';
    return [
      'Pastefromgdocs' => [
        'label' => t('Pastefromgdocs'),
        'image' => $path . '/pastefromgdocs.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
