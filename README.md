CKEditor Paste from Google Docs
====================

INTRODUCTION
------------

This module integrates the [Paste from Google Docs](
https://ckeditor.com/cke4/addon/pastefromgdocs) CKEditor plugin for Drupal 8.

This plugin allows you to paste content from Google Docs and maintain original content formatting.

Paste from Google Docs retains the following features:

    Text formatting
        Text and background colors
        Font family, style and size
        Basic formatting (bold, italic, underline)
        Font effects (strikethrough, superscript, subscript)
        Heading levels
        Text alignment
    Lists
        Numbered and bulleted lists
        Multilevel lists
        Different numbering formats (Roman, decimal, alphanumeric)
        Custom start number (e.g. you can start the list from number 4)
    Tables
        Borders and shading
        Cell size (width and height)
        Cell alignment
        Text formatting (as listed above)
    Images


REQUIREMENTS
------------

* CKEditor Module (Core)

* CKEditor Pastefromword

* CKEditor Pastetools

INSTALLATION
------------

### Install via Composer (recommended)

If you use Composer to manage dependencies, edit composer.json as follows.

* Run `composer require --prefer-dist composer/installers` to ensure you have
the composer/installers package. This facilitates installation into directories
other than vendor using Composer.

* In composer.json, make sure the "installer-paths" section in "extras" has an
entry for `type:drupal-library`. For example:

```json
{
  "libraries/{$name}": ["type:drupal-library"]
}
```

* Add the following to the "repositories" section of composer.json:

```json
{
  "type": "package",
  "package": {
    "name": "ckeditor/pastefromgdocs",
    "version": "4.13.0",
    "type": "drupal-library",
    "extra": {
      "installer-name": "ckeditor/plugins/pastefromgdocs"
    },
    "dist": {
      "url": "https://download.ckeditor.com/pastefromgdocs/releases/pastefromgdocs_4.13.1.zip",
      "type": "zip"
    }
  }
}
```

* Run `composer require 'ckeditor/pastefromgdocs:4.13.0'` to download the plugin.

* Run `composer require 'drupal/ckeditor_pastefromgdocs:^1.0.0'` to download the
CKEditor Paste from Google Docs module, and enable it [as per usual](
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).


### Install Manually

* Download the [CKEditor Paste from Google Docs](https://ckeditor.com/cke4/addon/pastefromgdocs)
CKEditor plugin.

* Extract and place the plugin contents in the following directory:
`/libraries/ckeditor/plugins/pastefromgdocs`.

* Install the CKEditor Paste from Google Docs module [as per usual](
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).

### Configuration

* Go to /admin/config/content/formats
* Choose the format that it should be enable.
* Add to the toolbar the 3 buttons:
pastetools, pastefromword, pastefromgdocs

MAINTAINERS
-----------
Current maintainers:

 * Julien de Nas de Tourris ([julien](https://www.drupal.org/u/julien))
